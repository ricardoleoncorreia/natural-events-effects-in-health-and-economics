# Natural events effects in health and economics

## Synopsis

Every year natural disasters produce a wide variaty of loses. Two of
them are the subject to study in this report, injuries and fatalities
and properties and crops damages. Using R libraries to process data
(dplyr) and plot results (ggplot2) will help us to find out the most
harmful event and the one with the greatest economics consequences.
At the end, we get that tornados causes the most injuries and fatalities
of all events and floods causes the greatest economics consequences.

The report is part of the projects of the [Data Science Specialization](https://www.coursera.org/specializations/jhu-data-science)
courses at Johns Hopkins University.
